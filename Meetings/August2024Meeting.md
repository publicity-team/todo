# Meeting log
[debian-publicity.2024-08-28-17.32.html](https://meetbot.debian.net/debian-publicity/2024/debian-publicity.2024-08-28-17.32.html)
[debian-publicity.2024-08-28-17.32.log.html](https://meetbot.debian.net/debian-publicity/2024/debian-publicity.2024-08-28-17.32.log.html)
[debian-publicity.2024-08-28-17.32.log.txt ](https://meetbot.debian.net/debian-publicity/2024/debian-publicity.2024-08-28-17.32.log.txt)
[debian-publicity.2024-08-28-17.32.txt  ](https://meetbot.debian.net/debian-publicity/2024/debian-publicity.2024-08-28-17.32.txt)


# Meeting agenda

## Debian Publicity Team ToDo/Meeting Points
 August 2024

#### Ongoing

   * Create and publish a new DPN/DPB bi- monthly.
       * Lets set up deadline dates. (team)
   * Share media reach analytics each month (anupa).
   * Start the habit of bi-monthly team meetings. (donald)
   * Create a shared calendar to note our important deadlines (next  point release / events / meetings) and our unavailability periods. 



#### In Progress

   * Shifting some media/outreach tasks across the team. (donald)
   * Complete the bits.d.o tag audit (donald)
   * Review with each member their 'tasks' from the area of focus list
   * The official Debian Publicity Style guide start.
   * Move our publishing guidelines and gotchas to more prominent areas for contributors and team use. 
   * Stage the testing and announcements of publicity team services:
       * rally.debian.net  (testing/not announced)
       *  pad.debian.net. - announced)
       * cryptpad.debian.net (testing/not announced)
       * event.debian.net (testing/not announced) (get 'events' from aba for the domain name)
       * weblate.debian.net  (testing/not announced)




#### Completed

   * Reach out to the translation teams to see how we can get work to them faster or publish their local works. (completed - Charles, Paulo(phls), \& Donald)
   * Solve the problem of double posting from micronews on some feeds (Paulo-phls)




#### Discussion



   * Recap of what worked and what did not work with our coverage of DebConf this year.
   * How do we strengthen collaboration with the Release and Webwml teams around the releases?
   * ToDo and Agendas need to be stationary and accessible. 
   * Re-incorporate https://wiki.debian.org/DeveloperNews for smaller items. Perhaps with micronews.



#### Planned

   * Start the reach out discussion with local teams to ease communication and find out about their events
   * Start the process of an audit for all of our wiki.d.o pages which are all vastly out of sync.
   * Actively ask for help with scripting via filing requests against our pseudo 'publicity' package:
       * bits script update (what needs updating?)
       * write script to automate conversions .md <-> .wml (Bits/News)
       * micronews update (Can 2 links be submitted?)
       * new services scripts (submit entries to event.d.n.)
   * Share and update our workflow so everyone knows exactly how our publishing pipeline works. (team/donald)




#### We be alive 

   * Shift internal team communication to matrix clients/instances to provide logs and to help keep everyone on the same page.  We have a bridge running irc <-> matrix - Paulo
   * Figure out how to attract new team members and contributors. Always invite!
   * Reach out to DDs and Contributors who are aware of the publicity services but are not using them.. This may a good lead in to restarting the DPN/DPB newsletters.
   * Create a place where Teams/DDs/contributors could upload propositions for messages (micronews, news, blog post)  or ask help to spread a news




#### **Notes**

I think we should consider moving back to a slower system of publishing where items are held for a day for the final pass review and THEN the publishing. We had that system strongly in place prior but not so much now. This would also able the translation teams catch up to the work, especially as we recently reached out to those teams and we cannot shut them out accidentally any futher.  - Donald



This means that we must also better anticipate our publications (for predictable events (e.g. DebConf, Debian's day… ) - Jean-Pierre



In case I have connection issues Jean-Pierre will chair this meeting or in his absence Paulo. 

