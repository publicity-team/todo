# Meeting log
[debian-publicity.2025-01-08-16.00.html](https://meetbot.debian.net/debian-publicity/2025/debian-publicity.2025-01-08-16.00.html)
[debian-publicity.2025-01-08-16.00.log.html](https://meetbot.debian.net/debian-publicity/2025/debian-publicity.2025-01-08-16.00.log.html)
[debian-publicity.2025-01-08-16.00.log.txt](https://meetbot.debian.net/debian-publicity/2025/debian-publicity.2025-01-08-16.00.log.txt)
[debian-publicity.2025-01-08-16.00.txt](https://meetbot.debian.net/debian-publicity/2025/debian-publicity.2025-01-08-16.00.txt)



# Meeting agenda

## Debian Publicity Team ToDo/Meeting Points
January 2025

### This will be an at length meeting to date set up so that we
can clear all agenda items. This lengthy meeting will allow our team to close
agenda items rather than leaving them for the next meeting.

* Confirm if we would like a meeting every month or every 2 months. Schedule said

* Review of the tasks assigned to team members. Continue updating our publishing
and procedures. 

* Create a schedule to note our important deadlines such as point releases,
events, and meetings. Also make public our unavailability periods for some
timezones.

* Ensure coverage for the next point release of Debian 12.9 (2025-01-11).

### Goals for 2025!

* Set up authors and deadline date for the DPN/DPB.

* Review procedures for monitoring point release announcements to clarify our
  relationship with webwml/release teams. Ask where exactly we are needed.

* Increase cooperation with the Webmasters team. (Do we need to be at the
forefront of releases at this point?).

* Reporting back to the project about our email, web, and social media reach.
  Aware the rest of the project of the large userbase input that is occurring
  and how DDs and Teams and access that direct feedback.

### Let us talk about

* Our new markdownlint procedures and how it can assist the team.

* Information about rally.debian.net (testing/not announced)

* Information about cryptpad.debian.net (testing/not announced)
This has moved to a new host and needs to be set up again. (donald)

* The idea to create a place where Teams/DDs/contributors can upload
  propositions to spread a information, news, or updates.

* Where can we fit in DevNews? 

* Renaming Master to Main


* How can we continue to be Awesome?


#### In Progress

* bits.d.o tag audit. (donald/team/open))

* DPN year end archive issue. (team/open)

* Outreach: We are sharing a VPS with the outreachy team for webpage updating. (donald)

* Fix DNS records for symposia.d.n., event.d.n., and cryptpad.d.n, (donald)

* publicity repository cleanup (donald/team/open)

