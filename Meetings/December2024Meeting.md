# Meeting log

[debian-publicity.2024-12-02-17.05.html](https://meetbot.debian.net/debian-publicity/2024/debian-publicity.2024-12-02-17.05.html)
[debian-publicity.2024-12-02-17.05.log.html](https://meetbot.debian.net/debian-publicity/2024/debian-publicity.2024-12-02-17.05.log.html)
[debian-publicity.2024-12-02-17.05.log.txt](https://meetbot.debian.net/debian-publicity/2024/debian-publicity.2024-12-02-17.05.log.txt)
[debian-publicity.2024-12-02-17.05.txt](https://meetbot.debian.net/debian-publicity/2024/debian-publicity.2024-12-02-17.05.txt)

# Meeting agenda

## Debian Publicity Team ToDo/Meeting Points
 December 2024

* Start the habit of bi-monthly team meetings

* Review of the tasks assigned to the team. Share and update our workflow so
  everyone knows exactly how our publishing pipeline works. Specify who
  can −and want− do what - procedures and permissions 
    Announcements and News
    Bits
    Micronews
    DPN/DPB
    Debian Timeline

* Create schedule to note our important deadlines (next  point release /
  events / meetings) and our unavailability periods (jipege)
    * Next point release (Debian 12.9 2025-01-11)
    * Reminder of DebConf 25 Logo and Artwork call for proposals (open
      until 15 december)

* Project(s) for 2025!

* Create and publish a new DPN/DPB bi- monthly. Lets set up deadline
  dates. (team)

* Review procedures for monitoring point release announcements to clarify our
  relationship with webwml teams (jipege)

* Solve the problem of double posting from micronews on some feeds: how to have
  a post about Bits publication on Micronews (Paulo-phls)

* Bits for Debian: an information point on markdownlint and markdownlint-2024

* Information about rally.debian.net (testing/not announced)

* Information about cryptpad.debian.net (testing/not announced)

* Create a place where Teams/DDs/contributors could upload propositions for
  messages (micronews, news, blog post)  or ask help to spread a news


#### In Progress

* Complete the bits.d.o tag audit (donald)
* Fix the markdown failure on Bits
