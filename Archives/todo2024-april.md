 # April 2024 Debian Publicity Team ToDo 
 

## Ongoing tasks
- Share media reach analytics each Friday


## Tasks priority I
-  Move pump.io instance from larjona's home server to the new VPS
   The issue here is the software used to push to pump.io is dated
   we should look in the ActivityStreams API directly to continue
   publishing to indenti.ca


## Tasks priority II 

- Create and share a 'Bits from the Publicity team' post.(donald/open)
- Monthly email to -project sharing emails received to press@d.o mailbox and 
  the occasional social media comments. (jipege)
- Complete the updated Debian version and point releases guidelines for Delegates/Team members. (donald)
- Update the tags used on bits.d.o, update some articles and posts to make them easier to find. 
- Update the overall Debian wiki Publicity Team page, update the services we 
  push to on social media. https://wiki.debian.org/Teams/Publicity 


# In Progress 
- Research the ActivityStream API / or something else for pump.io
- The Debian Yugo project (tarzeau)
- Start prepraton for DebConf24


# Discussions
- Test and find the best interview format for future interviews. 
- Establish guidelines for editing of the transcripts regarding grammar
- Do we pay for the full use of the transcription service?

# Notes
It is easier to use the dlvrt service to push the news out to social media
channels. Most of the API are complex and some have an outrageous cost for
access far past what we require for statistical information only.


# Completed
- Acquire and set up the press.debian.net VPS (donald and phls)
- Start the new 'Meet your Developers' interviews. (disaster2life)
- Set up dudle (scheduler) on VPS (phls)
- Set up etherpad on VPS (phls)
-  Organize the TODO repository (open)
- Create template for the DPB monthly (donald)
- Housekeeping the directories & structure in the bits repo. (donald)
- Update the bits repo README.md (donald)
