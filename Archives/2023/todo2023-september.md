PUBLICITY-TODO
# Debian Publicity Team ToDo

## September 2023
---
# Ongoing
Create and publish a new DPB each month
Share media reach analytics each Friday

# Planned

# In Progress
 - Work on the DPNhtml2mail.pl script error when using it to process the DPB
 - Work on the CRITICAL: 
    UndefinedError: 'articles_page' is undefined
    make: *** [Makefile:79: mpublish] Error 1

 - Create template for the DPB monthly
 - DebConf23 coverage

**Discussions**
===

**Notes**
===
- Start documenting errors and work arounds for errors, then fix said errors
- when drafting in micronews, add.py will stash the draft and publish both copies. 
- Workaround for the dillon error is to comment out line #79 in the Makefile.

# Completed
Published the Debian 30 years birthday recap. 
