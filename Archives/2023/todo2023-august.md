PUBLICITY-TODO
# Debian Publicity Team ToDo

## August 2023
---
# Ongoing
Create and publish a new DPB each month
Share media reach analytics each Friday

# Planned

- Write or take contributions from Interview Bill Mitchel for Debian History - [ ] Joost van Baal
- Complete the [Debian PointRelease](https://wiki.debian.org/Teams/Publicity/PointRelease) guide. -Donald Norwood
- Complete the [Debian VersionRelease](https://wiki.debian.org/Teams/Publicity/VersionRelease) guide. - Donald Norwood


# In Progress
 - Work on the DPNhtml2mail.pl script error when using it to process the DPB
 - Create template for the DPB monthly
 - Add the Debian 30 years birthday outside links to https://wiki.debian.org/PressCoverage


#### Reconfigure workflow guidelines

- Include more teams into the review process for news items, especially teams/individuals whose work we are mentioning.
- Increase the time to publish for items allowing for a better review time for typos or corrections. 


**Discussions**
===
- Do we also push the DPB to the news mailing list? (yes in progress)
- Where can the weekly data be posted or stored so it can be archived but not exposed to public mailing lists. 
- Include Developers or Teams that we reference in the review process.  (yes in progress)

**Notes**
===
- Start documenting errors and work arounds for errors, then fix said errors

- Problem: ~~- Drafts should stay in the drafts folders, never in the live folders.~~
    - Solution: When working with pelican and the status is draft, one may make html && make serve then browse to 127.0.0.1/drafts which will show all of the available drafts in the repo. Very useful for editing and previewing.

 - When hand writing the DPB: 
 - [ ] < b > tags must be outside of the < p > tags
  - [ ] The "Want to continue reading ..." section  is also added by the script (if used)
 - [ ]  Security advisories need not be added if using the script which adds its own. 
-  [ ] Substitue DPB for DPNhtml2mail
- [ ] < !-- text for notes or information can be written like this --> (remove space between 1st < !>)


  

# Completed

 -  Publish the Debian Project Bits on bits.debian.org - Team
 - Fix errors in bits tag pages. - Grzegorz Szymaszek, Charles, and team.
 - Publish the French translation of the DPB on bits.d.o. - Jean-Pierre
 - Interview Bill Mitchel for Debian History - [ ] Joost van Baal
 - Added a few icons to the Publicity repo. Icons are all from https://openclipart.org.
-  Published the media analytics in IRC. 2023-08-26.


#### DebianDay campaign 
>
> Debian Day Posting across media platforms. 
>> Days 5 away post **--in progress**  : _Draft located in the micronews/drafts repo._
>> 
>> Days 4 away post **--in progress**  : _Draft located in the micronews/drafts repo._
>>
>> Days 3 away post **--in progress**  : _Draft located in the micronews/drafts repo._
>>
>> Days 2 away post **--in progress**  : _Draft located in the micronews/drafts repo._
>>
>> Days 1 away post **--in progress**  : _Draft located in the micronews/drafts repo._
>>
>> Day of event post for bits.debian.org **--in progress** : _Draft locatedn  in bits/drafts folder._
>>> 30 years of Debian Bits article created and open for review 
>>>
>>>30 years of Debian  Bits article artwork requested, placed, artist credited

#### Media Consolidation
-  ~~Obtain moderator access for the r/debian subforum on reddit. - Donald Norwood~~

     Established r/debianproject instead which will take feeds from micronews. This leaves the user /r/debian subforum alone which is likely better for user discussions though not under our control.

     This was seemlingly impossible to do, it required a lot of give to the closed ecosystem: Emails, offers for PGP signed emails, direct messages on that platform and direct messages from our confirmed accounts to their accounts on other systems (X/Twitter) were ignored or segmented to AI/Bots. 

- ~~Push micronews to the r/debian subform.~~

     Add /r/debianproject on reddit to the dlvr.it media auto-poster.


#### Fixes: 
Merge Grzegorz Szymaszek commit https://salsa.debian.org/publicity-team/bits/-/commit/9da5917774447c017c4dc74833b341cf3efd2c53 to address page generations without pagination. 
