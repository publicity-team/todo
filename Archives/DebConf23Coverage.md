#### This is a copy of the workind pad used for DebcConf 23
#### Originally located at: https://pad.riseup.net/p/DebConf23_publicity_coverage#L20 



News to be published in Micronews regarding DebConf23

Things to remember: 
    * If you use a "#" symbol in the first word of Micronews, add a space before it. Else markdown identifies it as heading. eg: " #debconf23 is in Kochi."
    * two URLs in the same micronews causes issues with the bridge for twitter/X (only one of them is shown there) :/
    * Use escape character '\' before the symbols like " between micronews. 
    * To escape a single "!" on a line, instead of quotes use single quotes to stop bash expansion. 

Hashtags to use: #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia

Announce events
------------------------------------ 

Saturday 16 September








8. Thanks message


Debian thanks particularly our DebConf23 Platinum Sponsors Infomaniak, Proxmox, and Siemens https://debconf23.debconf.org/sponsors/ #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

Debian thanks the commitment of numerous sponsors to support DebConf1  https://debconf23.debconf.org/sponsors/ #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

Thank you to all the people who made #DebConf23 possible! The DebConf organisation team, Debian Video Team, our speakers, volunteers, and attendees. Would you like to join for organising the next DebConf? If so visit https://wiki.debian.org/DebConf/ and contact the DebConf team.

9. Group photo

10. DebConf closes announcement

To be published in www.d.o/News/2023 and bits.d.o (this one including photo, or updated when it's ready to include it)
References: https://debconf23.debconf.org/statistics/ and #debconf and #debconf-team (closing ceremony). Also last year's one https://www.debian.org/News/2022/20220724


First Draft

<define-tag pagetitle>DebConf23 closes in Kochi and DebConf24 dates announced</define-tag>

<define-tag release_date>2023-09-17</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<p>
Today, Sunday 17 September 2023, the annual Debian Developers
and Contributors Conference came to a close.
Hosting more than 325 ****arrived*** attendees from 31 different countries over a 
combined 89 event talks, discussion sessions, 
Birds of a Feather (BoF) gatherings, workshops, and activities,
<a href="https://debconf23.debconf.org">DebConf23</a> could have been regarded as a success if all the attendees had not been deeply affected by the lost of a member of our community, Abraham Raji. [The Debconf suspends its activities to bring support and express its compassion to his family and friends.]
</p>

<p>
The conference was preceded by the annual DebCamp held 3 September to 9 September
which focused on individual work and team sprints for in-person collaboration
towards developing Debian. In particular, this year there have been sprints to
advance development of 
#Mobian/Debian on mobile, reproducible builds and Python in Debian, 
and a BootCamp for newcomers, to get introduced to Debian and have
some hands-on experience with using it and contributing to the community.

<p>
The actual Debian Developers Conference started on Sunday 10 September 2023.
Together with activities such as the traditional 'Bits from the DPL' talk,
the continuous key-signing party, lightning talks and the announcement of next year's DebConf
(<a href="https://wiki.debian.org/DebConf/24">DebConf24</a> in XXXXXX, Israel), 
there were several sessions related to programming language teams such as Python, Perl and Ruby, 
as well as news updates on several projects and 
internal Debian teams, discussion sessions (BoFs) from many technical teams
(Long Term Support, Android tools, Debian Derivatives, Debian Installer and Images team,
Debian Science...) and local communities (Debian Brasil, Debian India, the Debian Local Teams), 
along with many other events of interest regarding Debian and free software.
</p>

<p>
The <a href="https://debconf23.debconf.org/schedule/">schedule</a>
was updated each day with planned and ad-hoc activities introduced by attendees
over the course of the entire conference. Several activities that couldn't be 
organized in past years due to the COVID pandemic returned to the conference's
schedule: a job fair, open-mic and poetry night, the traditional Cheese and Wine party,
the group photos and the Day Trip.
</p>

<p>
For those who were not able to attend, most of the talks and sessions were 
recorded for live streams with videos made,
available through the
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2023/DebConf23/">Debian meetings archive website</a>.
Almost all of the sessions facilitated remote participation via IRC messaging 
apps or online collaborative text documents.
</p>

<p>
The <a href="https://debconf23.debconf.org/">DebConf23 website</a>
will remain active for archival purposes and will continue to offer
links to the presentations and videos of talks and events.
</p>

<p>
Next year, <a href="https://wiki.debian.org/DebConf/24">DebConf24</a> will be held in XXXX, Israel,
from  XXXXX to XXXXX, 2024. 
#As tradition follows before the next DebConf the local organizers in Israel 
#will start the conference activites with DebCamp (September 03 to September 09, 2023),
#with particular focus on individual and team work towards improving the 
#distribution.
</p>

<p>
DebConf is committed to a safe and welcome environment for all participants.
See the <a href="https://debconf23.debconf.org/about/coc/">web page about the Code of Conduct in DebConf23 website</a>
for more details on this.
</p>

<p>
Debian thanks the commitment of numerous <a href="https://debconf23.debconf.org/sponsors/">sponsors</a>
to support DebConf23, particularly our Platinum Sponsors:
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://www.proxmox.com/">Proxmox</a>
and <a href="https://www.siemens.com/">Siemens</a>

</p>

<h2>About Debian</h2>

<p>
The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
<q>universal operating system</q>.
</p>

<h2>About DebConf</h2>

<p>
DebConf is the Debian Project's developer conference. In addition to a
full schedule of technical, social and policy talks, DebConf provides an
opportunity for developers, contributors and other interested people to
meet in person and work together more closely. It has taken place
annually since 2000 in locations as varied as Scotland, Argentina, and
Bosnia and Herzegovina. More information about DebConf is available from
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>About Infomaniak</h2>
<p>
<a href="https://www.infomaniak.com">Infomaniak</a> is a key player in the European cloud market and the leading developer of Web technologies in Switzerland. It aims to be an independent European alternative to the web giants and is committed to an ethical and sustainable Web that respects privacy and creates local jobs. Infomaniak develops cloud solutions (IaaS, PaaS, VPS), productivity tools for online collaboration and video and radio streaming services.
</p>

<h2>About Proxmox</h2>
<p>
<a href="https://www.proxmox.com/">Proxmox</a> develops powerful, yet easy-to-use open-source server software. The product portfolio from Proxmox, including server virtualization, backup, and email security, helps companies of any size, sector, or industry to simplify their IT infrastructures. The Proxmox solutions are based on the great Debian platform, and we are happy that we can give back to the community by sponsoring DebConf23.
</p>

<h2>About Siemens</h2>
<p>
<a href="https://www.siemens.com/">Siemens</a> is technology company focused on industry, infrastructure and transport. From resource-efficient factories, resilient supply chains, smarter buildings and grids, to cleaner and more comfortable transportation, and advanced healthcare, the company creates technology with purpose adding real value for customers. By combining the real and the digital worlds, Siemens empowers its customers to transform their industries and markets, helping them to enhance the everyday of billions of people.
</p>


<h2>Contact Information</h2>

<p>For further information, please visit the DebConf23 web page at
<a href="https://debconf22.debconf.org/">https://debconf23.debconf.org/</a>
or send mail to &lt;press@debian.org&gt;.</p>


1. Confirmed events: 
    Confirmed events for DebConf23 is on our website https://debconf23.debconf.org/talks/ #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia

2. Cheese and Wine announcement:
    Call for Cheese and Wine party at DebConf Kochi. https://lists.debian.org/debconf-announce/2023/08/msg00002.html #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia

3. Schedule: Combined with another as we got really late with this.
    Schedule for DebConf23 in Kochi, India has been published: https://debconf23.debconf.org/schedule/ #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia
    
4. Siemens Press release:
    Siemens: Our Platinum sponsor for DebConf23 Kochi https://bits.debian.org/2023/09/debconf23-siemens-sponsor.html #debian #siemens #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

5. DebCamp starts today! https://wiki.debian.org/DebConf/23/DebCamp #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

6. Events during DebCamp

Keep an eye on these:
    https://wiki.debian.org/DebConf/23/DebCamp
    https://wiki.debian.org/Sprints
(if they are announced elsewhere, check with the team if they want micronews)

New to Debian? BootCamp, as part of DebCamp23, is an opportunity to get introduced to #Debian https://lists.debian.org/debconf-announce/2023/09/msg00000.html #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia

If you plan to participe in the DebConf23 Key Signing party, please read the final instructions! https://lists.debian.org/debconf-announce/2023/09/msg00007.html #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

Ubuntu will be at DebConf23! https://ubuntu.com//blog/ubuntu-at-debconf-2023 #ubuntu #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

https://deb.li/kZP7


Sunday 10 September

7. DebConf23 starts on this Sunday! Most of the talks are streamed online. Watch out this space for our schedule: https://debconf23.debconf.org/schedule/ Live talks can be watched by clicking on the venue links in schedule. #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

To be announced after the bits comes out:
DebConf23 starts today! Thanks to all our sponsors for DebConf23 Kochi: https://bits.debian.org/2023/09/debconf23-welcomes-sponsors.html #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

------ starts today bits post (draft)----
DebConf23 starts today in Kochi On Sun 10 September 2023 with tags debconf debconf23 

Written by The Debian Publicity Team

DebConf23, the 24th annual Debian Conference, is taking place in Kochi, India from September 10th to 17th, 2023.

Debian contributors from all over the world have come together at Infopark, Kochi to participate and work in a conference exclusively run by volunteers.

Today the main conference starts with over 373 attendants expected and 92 activities scheduled, including 45-minute and 20-minute talks and team meetings ("BoF"), workshops, and a job fair, as well as a variety of other events.

The full schedule at https://debconf23.debconf.org/schedule/ is updated every day, including activities planned ad-hoc by attendees during the whole conference.
If you want to engage remotely, you can follow the video streaming available from the DebConf23 website of the events happening in the three talk rooms: Anamudi, Kuthiran and Ponmudi. Or you can join the conversation about what is happening in the talk rooms:  #debconf-anamudi,  #debconf-kuthiran and  #debconf-ponmudi (all those channels in the OFTC IRC network) and #debconf for common discussions related to DebConf.

You can also follow the live coverage of news about DebConf23 on https://micronews.debian.org or the @debian profile in your favorite social network.
DebConf is committed to a safe and welcoming environment for all participants. See the web page about the Code of Conduct in DebConf23's website for more details on this.
Debian thanks the commitment of numerous sponsors to support DebConf23, particularly our Platinum Sponsors: Infomaniak, Proxmox and Siemens.
-----------------

**SUNDAY - September 10: **

Bits from Debian - Regarding DebConf start.  (To be published before the opening ceremony preferably) 
** Post in draft status here: https://deb.li/3yirV)
 

(Publish before 4.30 UTC) -> DebConf23 starts with the \"Opening ceremony\" at 10.30 AM IST(05:00 UTC) in Venue 1: Anamudi. Watch it online at https://debconf23.debconf.org/schedule/venue/1/ followed by \"Continuous Key-Signing Party introduction\" (no video streaming) from 11.00 AM IST(5.30 UTC) #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia


(Publish before 5.30 UTC) -> At 11.30 IST (6.00 UTC) the talks in various halls are: \"Software Heritage: building a community to safeguard the Software Commons\" in Anamudi, \"Why Debian\" in Kuthiran and \"Debian Treasurer BoF\" in Ponmudi https://debconf23.debconf.org/schedule/ #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

(Publish at 7.45 UTC) -> After lunch DebConf23 resumes with the Job Fair from 13:30 IST (8.00 UTC) through 15.30 IST (10.00 UTC) on the second floor of the Four Points hotel in the hallway near Ponmudi. https://debconf23.debconf.org/schedule/job-fair/

(Publish before 9.30 UTC) 3.00 PM IST ->  At 15.30 IST (10:00 UTC) afternoon session start with talks in Anamudi \"Using FOSS to fight for Digital Freedom\", \"Teams, newcomers and numbers\" in Kuthiran, and \"State of Stateless - A Talk about Immutability and Reproducibility in Debian\" in Ponmudi. https://debconf23.debconf.org/schedule/ #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

(Publish before 10.30 UTC) 4.00 PM IST -> At 16:30 IST (11:00 UTC) we have three short talks: \"Community building in Free Software and focusing on DEI\" in Anamudi, \"Digital security with Free Software\" in Kuthiran, and in Ponmudi \"What's missing so that Debian is finally reproducible?\" https://debconf23.debconf.org/schedule/?block=1  #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

(Publish before 11.00 UTC) 4.30 PM IST -> There are three additional short talks before the afternoon break, at 17:00 IST (11:30 UTC): \"Debian: Resistance is Futile\" in Anamudi, \"Forming a SSH Cluster using old smartphones (Mobian and other OSes) - Sustainable Computing\" in Kuthiran, and the usual \"Hello from keyring-maint\" in Ponmudi https://debconf23.debconf.org/schedule/ #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

(Publish before 12.00 UTC) 5.30 PM IST -> The last set of talks for today at Debconf23: \"Empower Your Users to Manage custom Debian Repository\" in Kuthiran, and \"A beginner's guide to debian packaging\" in Ponmudi at 18:00 IST (12:30 UTC)  https://debconf23.debconf.org/schedule/ The talk \"DebianArt: Collaborating on Design and Art for Debian Campaigns\" in Anamudi has been cancelled and might be rescheduled for later. #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia

(Publish before 13.30 UTC) 7.00 PM IST -> The first day of DebConf23 has ended, we start again tomorrow, Monday 11, September at 10:30 IST (05:00 UTC). The schedule for the day will be https://debconf23.debconf.org/schedule/?block=2 Thank you to all our contributors, viewers, and to our Debconf Video team! See you tomorrow! #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia

**Monday - September 11:**
    
    18. The Debian Developers Conference continues today Monday 11, September, at 10:30 IST (05:00 UTC) - have a look at the schedule for the day https://debconf23.debconf.org/schedule/?block=2 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia
    
19. (Publish before 4.30 UTC) This morning begins with two talks \"Open Source Tools for Research\" in Anamudi and \"A declarative approach to Linux networking with Netplan\" in Kuthiran, and a workshop at Ponmudi \"Live packaging workshop\" at 10:30 IST (05:00 UTC) https://debconf23.debconf.org/schedule/?block=2 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia


20. At 11:30 IST (06:00 UTC), the short talk \"Quick peek at ZFS, A too good to be true file system and volume manager.\" is happening at Kuthiran and at 12.00 IST (6.30 UTC) the short talk \"GNOME community and ways to engage\" is happening at Anamudi. The talk at Anamudi from 11.30 IST (6.00 UTC), \"Mapping the Distribution of Gamma-Ray Bursts  using Open Data GIS\" has been cancelled. https://debconf23.debconf.org/schedule/?block=2 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

21. The short talk \"Migrating veteran FOSS projects to a modern workflow\" at 12.00 IST (6.30 UTC) in Kuthiran has been cancelled and the events at DebConf23 will resume after lunch at 15.30 IST (10.00 UTC). https://debconf23.debconf.org/schedule/?block=2 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

(Publish before 8:30 UTC) 14:00 IST -> Mobile friendly schedule for DebConf23: https://debconf23.debconf.org/schedule/mobile/ #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

23. (Publish before 9:30 UTC) 15:00 IST -> After the lunch, at 15:30 IST (10:00 UTC) in Anamudi, Jonathan Carter takes a look at the state of the Debian project in the usual \"Bits from the DPL\" To watch it online: https://debconf23.debconf.org/schedule/venue/1/ #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

24. (Publish before 10:15 UTC) 15:45 IST -> In Anamudi, there will be two short talks, \"Sustainable Digital Intervention in Tribal and Remote areas of Andhra Pradesh using Freedom Box\" at 16:30 IST (11:00 UTC) and \"Gomti: A collection of PLL-based True RNG on FPGA\" at 17:00 IST (11:30 UTC) before the afternoon break https://debconf23.debconf.org/schedule/?block=2 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

25. (Publish before 10:30 UTC) 16:00 IST -> The BoF \"Chatting with ftpmasters\" intended to smoothen the communication process between ftpmaster and developers will be held in Kuthiran from 16:30 IST to 17:20 IST (11:00 UTC to 11.50 UTC) https://debconf23.debconf.org/talks/31-chatting-with-ftpmasters/ This will also be live streamed. #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

26. (Publish before 12:00 UTC) 17.30 IST -> After the well-deserved afternoon break, get back at 18:00 IST (12:30 UTC) to Anamudi where Enrico Zini, in his talk \"Adulting\", wants to start figuring out how to address practical issues around the sustainability of the Debian community, in a way that fits the needs and peculiarities of the community. Watch the talk online at: https://debconf23.debconf.org/schedule/venue/1/ and the crowd at DebConf23 will move towards the traditional \"Cheese and Wine party\" after that. #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

**Tuesday - September 12:**

27. (Publish before 4:30 UTC)->9.30 IST -> The Debian Developers Conference continues today Tuesday 12, September, at 10:30 IST (05:00 UTC) - have a look at the schedule for the day. https://debconf23.debconf.org/schedule/?block=3 We remind you that you can watch most of the talks  streamed online by clicking on Venue names in the schedule. You can also watch the recorded talks published later. #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

28. (Publish at 4:30 UTC)-> 10.00 IST -> In the morning session, there are three talks \"Opportunity Open Source conference in the IIT Mandi, India: Motivating people to be a part of us!\" exposing the outcome and experiences from the conference held just before the DebConf, in Anamudi at 10:30 ITC (05:00 UTC), \"My life in git, after subversion, after CVS\" in Kuthiran, and the \"DebConf bursary team BoF\" meeting in Ponmudi.  https://debconf23.debconf.org/schedule/?block=3 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

29. (Publish at 5:30 UTC) 11.00 IST -> Beginning at 11:30 IST (06:00 UTC), We conclude the morning session with the talk: \"TuxMake with Debian\" in Ponmudi, while two BoFs will be held in Kuthiran,  \"Face-to-face Debian meetings in a climate crisis\", and in Chembra, \"EU Legislation BoF - Cyber Resilience Act, Product Liability Directive and CSAM Regulation\". The first two events can be watched online/ https://debconf23.debconf.org/schedule/?block=3 The talk, \"Debian Experience & Outreachy Project: Improve yarn package manager integration with Debian\" in Anamudi, has been cancelled and might be rescheduled later. #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

30. (Publish at 9:30 UTC) 3:00 IST -> After the lunch, at 15:30 IST (10:00 UTC) in Anamudi, you will have the opportunity to \"Meet the Technical Committee\" you can watch them on-line https://debconf23.debconf.org/schedule/venue/1/. You can also listen to two upcoming talks \"Automated vulnerability patching at scale in Debian\" in Kuthiran or \"My journey with homelabing with freenas, proxmox, pfsense and 2nd hand hardware...\" in Ponmudi #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

31. (Publish at 10:30 UTC) 4.00 IST -> Next set of events before the afternoon break, at 16:30 IST (11:00 UTC): the \"Debian Reimbursement Web Application\" in Anamudi, \"Home Automation using Free Software\" a workshop in Kuthiran, and the \"Salsa CI BoF\" in Ponmudi for those interested on CI https://debconf23.debconf.org/schedule/?block=3 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

32. (Publish at 12:00 UTC) 5:30 IST -> In the second half of the afternoon session, at 18:00 IST (12:30 UTC) in Anamudi, the amazing development of a local community in Brazil will be exposed in the talk \"From 5 to 100: The Debian Brasília Community's Story\", and in Ponmudi, the \"/usr-merge BoF\" will give a short update on the state of affairs of this difficult transition, while in Kuthiran two short talks, \"Turn your spare laptop or any other device as a server\" and \"How many mobile Linux developers does it take to ....\" will end the third day of Debconf23 https://debconf23.debconf.org/schedule/?block=3 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

33.(Publish after 12:00 UTC) 5:30 IST -> The third day of DebConf23 has ended. Tomorrow, Wednesday, September 13, it is Day Trip. Enjoy your Tour! 
DebConf23 next session starts again Thursday 14, September at 10:30 IST (05:00 UTC). The schedule for the day will be https://debconf23.debconf.org/schedule/?block=5 Thank you to all our contributors, viewers, and to our Debconf Video team! See you tomorrow! #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia

Are you attending #DebCamp23 and #DebConf23 in person, online, or at your local LUG? Do you have photos you would like to share? Please consider adding them to the DebConf share git-lfs repo: https://salsa.debian.org/debconf-team/public/share/debconf23. Note, this is a git-lfs repo, you'll have to initialize git-lfs before committing. See the README. We will compile and share as many of the memories and moments as we can. #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia


**Wednesday 13 September:**
Day Trip

**Thursday 14 September:**

All Talks Cancelled.

**Friday 15 September**

34. (Publish before 9:00  UTC) After the strong emotion caused by the tragic loss of a member of our community, Abraham Raji, the Debian Developers Conference resumes its work today Friday 15, September, at 15:30 IST (10:00 UTC) - have a look at the schedule for the day. We remind you that you can watch on line most of the talks We remind you that you can watch on line most of the talks: just click on the name of the venue in the schedule https://debconf23.debconf.org/schedule/?block=6 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

35. (Publish at 9:30 UTC) (5:30 AM EDT / 9:30 GMT / 15:00 IST) Opening the afternoon session, at 15:30 IST (10:00 UTC) in Anamudi, there is a talk that describes the current state of the \"Java and FIPS\" and shares some of Ubuntu FIPS plans for Java https://debconf23.debconf.org/schedule/?block=6 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia


36. (Publish at 10:30 UTC) (6:30AM EDT / 10:30 GMT / 16:00 IST) Begining at 16:30 IST (11:00 UTC), four talks will take place before the afternoon break: \"What's new in the Linux kernel (and what's missing in Debian)\" in Anamudi, \"Uplifting the gross roots using FOSS communities\" describes a decade work of an FOSS Community now powering up Debian\" in Kuthiran, \"A Guided Tour to Debian Installer Development\" guide you through the process of how to work on it in Ponmudi, while in Chembra \"Past and future changes to the Debian web pages\" are evoked https://debconf23.debconf.org/schedule/?block=6 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

37. (Publish at 12:00 UTC) (8:00AM EDT / 12:00 GMT / 17:30 IST)At 18:00 IST (12:30 UTC), five events conclude the afternoon session: the talk \"Isar - Building Images with the Power of Debian\" in Anamudi, the BoF \"use Perl; # Annual meeting of the Debian Perl Group\" in Kuthiran, in Ponmudi the \"DebConf committee BoF\" will discuss about this years and next years\' DebConf, the \"Eu Legislation workgroup\" will prepare a Debian statement regarding EUs Cyber Resilience Act in Chembra, while in hotel room 1019 Thomas Lange will led a BoF \"When do News become too old?\" about stripping old web pages https://debconf23.debconf.org/schedule/?block=6  The last two events won't have video coverage. #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

38. The sixth day of DebConf23 has ended, we start again tomorrow, Saturday 16, September at 10:30 IST (05:00 UTC). The schedule for the day will be https://debconf23.debconf.org/schedule/?block=7 Thank you to all our contributors, viewers, and to our Debconf Video team! See you tomorrow! #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia

39. (Publish before 4:30 UTC / 12:30 AM EDT) The Debian Developers Conference continues today Saturday 16, September, at 10:30 IST (05:00 UTC) - have a look at the schedule for the day. https://debconf23.debconf.org/schedule/?block=7 We remind you that you can watch most of the talks streamed online by clicking on Venue names in the schedule. You can also watch the recorded talks published later. #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

40. (Publish at 4:30 UTC / 12:30 AM EDT) To start the day, the talk \"Intro to SecureDrop, a sort-of Linux distro\" takes place in Anamudi at 10:30 IST (05:00 UTC), while two meetings are held: in Kuthiran the \"Debian Diversity BoF\" to check in with respect how we are doing and what we can improve on, and the annual \"Python BoF\" to discuss Python\'s future in Debian and how people can help, in Ponmudi https://debconf23.debconf.org/schedule/?block=7 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia


41. (Publish at 5:30 UTC / 01:30 AM EDT) For all those concerned by containers, two short talks follows one another in Anamudi: \"The Docker(.io) ecosystem in Debian\" at 11:30 IST (06:00 UTC), then \"Chiselled containers\" at 12:00 IST (06:30 UTC). At the same time, in Kuthiran, the talk \"Bits from Brazil\" reports about what Debian community have done in Brazil during 2023, and there is an \"Usability Report for the Debian Installation Process\" in Ponmudi, while the \"LXQt Team Bof\" is held in Chembra, followed by \"Freexian meet-up\" .https://debconf23.debconf.org/schedule/?block=7 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

42. (Publish at 6:30 UTC / 02:30 AM EDT) The Group photo of #DebConf23 will be taken today after the talks, before the lunch, between 12:30 and 12:45 IST. Please find the instructions on the mail: https://lists.debian.org/debconf-discuss/2023/09/msg00093.html #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia.

***** It was evoked the idea to call people to wear a shirt with a design made Abraham ******

43. (Publish at 9:30 UTC / 5:30 AM EDT) At 15:30 IST (10:00 UTC), the afternoon session begins with the BoF \"Debian Contributors shake-up\" to improve the knowlegde of Debian contributors, in Kuthiran. At the same time, two workshops start in Ponmudi, \"Live Coding tools on Debian: How to install and setup \" and in Chembra \"Debian Installer: Text Adventure\". They will end at afternoon break time https://debconf23.debconf.org/schedule/?block=7 The talk \"Digital Rights are Human Rights\" in Anamudi at 15:30 IST (10:00 UTC), has been cancelled. #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

44. (Publish at 10:00 UTC / 6:00 AM EDT) At 16:00 IST (10:30 UTC), in Anamudi, you have the opportunity to \"Meet (part of) the Ubuntu Server team\" or to attend the \"New Member BOF\" in Kuthiran https://debconf23.debconf.org/schedule/?block=7 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia #ubuntu

45. (Publish at 10:30 UTC / 6:30 AM EDT) In Anamudi, at 16:30 IST (11:00 UTC), the talk \"The New Architecture for Printing and Scanning on Debian\" introduces to the New Architecture and how it affects Debian, and in Kuthiran the \"Local Groups BoF\" is held until the afternoon break https://debconf23.debconf.org/schedule/?block=7 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia


46. (Publish at 12:00 UTC / 8:00 AM EDT) At 18:00 IST (12:30 UTC), in Anamudi, the talk \"Introduction to Proxmox Virtual Environment for home labs and beyond\" will be followed at 18:30 IST (13:00 UTC) by \"Map Kerala Initiative\". Two meetings will end the afternoon session: the \"Debian Brasil BoF\" in Kuthiran, and the BoF about \"Localization of Mobile Linux\" in Ponmudi. https://debconf23.debconf.org/schedule/?block=7 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

https://debconf23.debconf.org/schedule/venue/4/ <- Elsewhere. Looking now
https://debconf23.debconf.org/talks/111-poetry-event/
No real place, generally seems unannonuced. We can ask when he returns -Donald


47. Since 2012 there/'s the Poetry Event at DebConf tradition.  We gather and share and  recite poems, under the motto /“Let/’s bring back some written thoughts that are moving./”  See https://debconf23.debconf.org/talks/111-poetry-event/, remote participation possible via the oftc #debian-diversity irc channel and https://pad.dc23.debconf.org/p/111-poetry-event. Join us at 21.00 IST on the second floor across from the video team room.


The seventh day of DebConf23 has ended, we start again tomorrow, the final day of DebConf23, Sunday 17, September at 10:30 IST (05:00 UTC). The schedule for the day will be https://debconf23.debconf.org/schedule/?block=8 Thank you to all our contributors, viewers, and to our Debconf Video team! See you tomorrow! #debian #debconf #freesoftware #dc23 #debconf23 #kochi #debconfkochi #debianindia

**Sunday - September 17:**
48. (Publish before 4:30 UTC / 12:30 AM EDT) Today Sunday 17, September, is the last day of DebConf23, starting at 10:30 ITS (05:00 UTC)! Have a look at the schedule for the day. https://debconf23.debconf.org/schedule/?block=7 We remind you that you can watch most of the talks streamed online by clicking on Venue names in the schedule. You can also watch the recorded talks published later. #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia


49. (Publish at 4:30 UTC / 12:30 AM EDT) At 10:30 IST (05:00 UTC) the morning session begins with the talk \"Debian Experience & Outreachy Project: Improve yarn package manager integration with Debian\" in Anamudi, and three BoFs: \"Debian.net team BoF\" in Kuthiran, \"Live Coding for art, sound and visuals\"  for people interested on live coding to produce sound and visuals in Ponmudi, and the \"Debian Med BoF\" in Chembra https://debconf23.debconf.org/schedule/?block=8 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia


50. (Publish at 5:30 UTC / 1:30 AM EDT) At 11:30 IST (10:00 UTC), the talk \"The year of Linux On Desktop^WMobile\" in Anamudi, and two BoFs, the \"debian.social BoF\" in Kuthiran and \"Ruby team BoF\", in Ponmudi will end the session https://debconf23.debconf.org/schedule/?block=8 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

51. (Publish at 9:30 UTC / 5:30 AM EDT) After lunch, at 11:30 IST (10:00 UTC) in Anamudi, the session \"Live Demos & Lightning Talks\" is the opportunity to show off recent work https://debconf23.debconf.org/schedule/?block=8 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

52. (Publish at 10:30 UTC / 6:30 AM EDT) The \"DebConf 24 BoF & DebConf 25: In your city?\", the quite traditional meeting between DebConf N and DebConf N+1 teams, will be held at 16:30 IST (11:00 UTC) in Anamudi, in the presence of the DebConf committee. The possible locations for DebConf25 will be evoked! If you’re interested in hosting DebConf in your city, this is the time to come and present your ideas! https://debconf23.debconf.org/schedule/?block=8 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia


53. (Publish at 12:00 UTC / 08:00 AM EDT) At 18:00 IST (12:30 UTC) in Anamudi, we have our #DebConf23 \"Closing Ceremony\": Some final statistics and goodbye to Kochi.
Thank you all for coming to DebConf23! https://debconf23.debconf.org/schedule/?block=8 #debian #debconf #debconf23 #freesoftware #dc23 #kochi #debconfkochi #debianindia

