# Debian Publicity Team
## Team member focus points

<!---
Alt title: Team member ownership matrix?
-->

***August 2024 ->***      

#### Paulo Santana (phls): 
* Translations: Portuguese
* VM Instance Administration
* VM Integration of team services
* Delegate shadowing


#### Ananthu C V (weepingclown): 
 * Editing and review
 * Style guidelines
 * DebianDay31 Anniversary Pictures compilation
  

#### Yashraj Moghe (disaster2life):
* Interviews of DDs and Contributors
* Editing and review
* Calendar highlighter!


#### Anupa Ann Joseph (anupa):
* Delegated tasks
* Social media postings and monthly analytics
* User Forum thread markings
* Outreach


#### Emmanuel Arias (eamanu):
* Social media postings
* User Forum thread markings
* Media tracking


#### Carlos Henrique Lima Melara (Charles): 
* Translations: Portuguese
* Editing and review
 * Triage
 * Website Housekeeping and Projects


#### Grzegorz Szymaszek (gsz):
* Translations: Polish
* Editing and review
* Website Housekeeping and Projects
 

#### Stefan Kropp (stefank):
* Website Housekeeping and Projects


#### Donald Norwood (Donald):
* Delegated tasks
* Editing and review
* Todo updates
* Meeting Agenda

#### Javier Fernández-Sanguino (jfs): 
* Translations: Spanish
* Editing and review
* Maintenance of project history

#### Jean-Pierre (jipege1):
* Delegated tasks
* Translations: French
* Editing, publishing, and review
* Debian Timeline and French review
* Meeting Agendas


#### Joost van Baal-Ilić (joost):
 * Delegated tasks
 * Editing and review


#### Alex Myczko (tarzeau):
 * dev.to bridge 

