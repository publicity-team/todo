# Publicity Team Repository Clean up

## Outstanding

#### Issues 
3 open all are within /events and active.

####  Merge requests
0 outstanding merge requests:
Jeremiah C. Foster: Updating the year in IDEAS page. (non-invasive - closed)
Birger Schacht: Add a new script that uses udd for getting a list on new maintainers. (emailed - closed)
Birger Schacht: New create rc stats json. (emailed -closed)

## Removal of repository commit email notifications
#### Why?
* mini-embargo of our information
	* stops websites and data-miners from the potential to pre-release information during the timeframe that we are preparing items for publication. 
* stopping unnecessary processes and overhead waste on project resources
* Most were last done by @donald 3 years ago on average

#### announcements
Email integration: No (was Yes)
IRC webhooks : Yes
*checking with adam/checked and it is fine. 

#### bits
Email integration: No (no change)
IRC webhooks : Yes (no change)

#### debian-history
Email integration: Yes (no change)
IRC webhooks : Yes (no change)

#### debian-timeline
Email integration: Yes (no change)
IRC webhooks : Yes (no change)

#### devnews 
 *Active 9 years ago*
Email integration: No (no change)
IRC webhooks : Yes (no change)

#### dpn
 *Active 2 years ago*
Email integration: Yes (no change)
IRC webhooks : Yes (no change)

#### events
Email integration: No (no change)
IRC webhooks : Yes (no change)

#### Interview
IRC webhooks : No (no change)
Email integration: No (no change)

#### mails
Email integration: No (no change)
IRC webhooks : No (was yes)

#### media-images
IRC webhooks : Yes (was no)
Email integration: No (no change)

#### micronews
Email integration: Yes (no change)
IRC webhooks : Yes (no change)

#### publicity-archive
*Renamed from 'publicity'*
Email integration: Yes (no change)
IRC webhooks : Yes (no change)

#### StyleGuide
IRC webhooks : Yes
Email integration: Yes 

#### Talks
IRC webhooks : Yes (no change)
Email integration: No (no change)

#### todo
IRC webhooks : Yes
Email integration: No 

#### Embargoed
* Email to salsa-admin about this repo *
IRC webhooks : No (no change)
Email integration: No (no change)


## Archived projects

#### mail
redundant as we have email form letters in the style-guide project
#### ian-meda
renamed to ian-murdock-media and moved to publicity-archives. 
#### events-src
original event.d.n source code. Current repo is events

## Deleted projects
#### refcard
*This is a fork 44 commits behind the Documentation Project/refcard - Why do we have this?*
*An email was sent to Frans Spiesschaert DD asking why it is housed here, no
need to move just curious. Answer: He did not have access to the other proper
repo at the time. Fork can be removed. 
IRC webhooks : No (no change)
Email integration: No (no change)
