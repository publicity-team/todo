 # January 2025 Debian Publicity Team ToDo 
 
## In Progress 
- Bits.d.o tags audit - Donald
- DPN year end issue - Team/Donald 
- Vorlon obituary - Team/Joost/Donald
- Apply feedback to rally.d.n. - Donald
- Fix cryptpad html issue - Donald


## Action items
- Update task matrix - Charles
- Create flat file Calendar - Anupa
- Pull social media metrics - Anupa
- Ask translation teams when they start translations for announcements - Charles
- Read DevNews to decide where it goes - Team

## Open items
- Update the overall Debian wiki Publicity Team page, update the services we 
  push to on social media. https://wiki.debian.org/Teams/Publicity - Anyone

## Discussions
- Use of Twitter
- Which other teams to reach out toward for more collaboration

## Notes
- 


## Completed
- Publicity-Team Salsa repository cleanup - Donald
 
