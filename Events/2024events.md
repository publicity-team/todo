**List of upcoming 2024 events and dates that we will start to raise awareness of prior to and during the events.**

&nbsp;

***Publicity Requirements***

* All of the dates for the event.
* The debian-mailing-list the event will use.
* The hashtags the event will use in addition to #debian and #debian-$event (i.e. #debian-miniconf)
* List of sponsors **and** the sponsor images for the bits.d.o postings.
* A point person for the event who can answer questions we may have and if needed approve things we may mention on the teams behalf.

&nbsp;

***Media Outreach*** _(these timeframes are a rough guideline for smaller events)_

Our coverage should begin around the time that the organizers are ready to open their registration queues.

* 2 weeks prior to the event (or when the post is made on the -list) we send it out via micronews/social.
* 1 week prior to the event we create a bits.d.o post.
* 1 week prior to the event a micronews/social post is sent framed around any last minute reminders for travel or needs. That post should link back to the bits.d.o post or that debian-list reminders thread.



&nbsp;

***Notes***

Smaller events may not need a constant push to the micronews service for details such a meeting times, conference rooms, or speaker information which is normally reserved for DebConf. We may only receive an open post for the event, the schedule for the day, and the closing post for the event.

Follow the event as best we can so that later we can use information for the _Events_ section of the DPN/DBP.

Not all teams will send reminders about their event, which is why it is critial that we know the entire event date range.

&nbsp;

***Events for 2024***

March 3rd-10th 2024: MiniDebCampHamburg

* https://lists.debian.org/debian-devel-announce/2023/12/msg00007.html


MiniDebConf Belo Horizonte (Brazil) 2024 - April 27 -30:

*	https://lists.debian.org/debian-devel-announce/2023/12/msg00005.html

MiniDebConf Berlin (Germany) 2024 - May 14 - May21: 

* https://wiki.debian.org/DebianEvents/de/2024/MiniDebconfBerlin


DebConf24 to be held in Busan, South Korea (Dates Sunday, July 21 - Sunday,
August 4 (15 days)):

* https://lists.debian.org/debian-devel-announce/2023/12/msg00002.html
* https://lists.debian.org/debconf-team/2024/01/msg00000.html


MiniDebConf Toulouse (France) 2024 - November 16 - 17

* https://wiki.debian.org/DebianEvents/fr/2024/Toulouse

